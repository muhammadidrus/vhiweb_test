import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vhiweb_test/providers/users.dart';
import 'package:vhiweb_test/screens/detail_screen.dart';

import 'package:vhiweb_test/screens/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Users(),
        )
      ],
      child: MaterialApp(
        title: 'Test IT - Flutter Developer',
        theme: ThemeData(
          primarySwatch: Colors.indigo
        ),
        home: HomeScreen(),
        routes: {
          DetailScreen.routeName: (ctx) => DetailScreen()
        }
      )
    );
  }
}
