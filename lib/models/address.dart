class Address {
  final String street;
  final String suite;
  final String city;
  final String zipcode;
  final Geo geo;

  Address({this.street, this.suite, this.city, this.zipcode, this.geo});
}

class Geo {
  final String lat;
  final String lng;

  Geo({this.lat, this.lng});
}