import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/foundation.dart';
import 'package:vhiweb_test/models/user.dart';

class Users with ChangeNotifier {
  List<User> _items = [];

  List<User> get items {
    return [..._items];
  }

  User findById(int id) {
    return _items.firstWhere((prod) => prod.id == id);
  }

  Future<void> fetchAndSetUsers() async {
    final _url = 'https://jsonplaceholder.typicode.com/users';
    try {
      final response = await http.get(_url);
      if (response.statusCode == 200) {
        List jsonResponse = json.decode(response.body);
        _items = jsonResponse.map((job) => new User.fromJson(job)).toList();
        notifyListeners();
      } else {
        throw Exception('Failed to load users from API');
      }
    } catch (error) {
      throw Exception('Failed to load users from API');
    }
  }
}