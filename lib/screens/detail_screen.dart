import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vhiweb_test/models/user.dart';
import 'package:vhiweb_test/providers/users.dart';
import 'package:vhiweb_test/widgets/content.dart';

class DetailScreen extends StatelessWidget {
  static const routeName = '/user-detail';
  
  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context).settings.arguments as int;
    User user = new User();
    user = Provider.of<Users>(
      context,
      listen: false,
    ).findById(id);

    Future<void> _refreshUser(BuildContext context) async {
      try {
        await Provider.of<Users>(context).fetchAndSetUsers();

        user = Provider.of<Users>(
          context,
          listen: false,
        ).findById(id);
      }
      catch(e) {
        user = user;
      }
    }

    _refreshUser(context);

    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          user.name,
          style: TextStyle(color: Colors.white, fontSize: 18.0),
        )
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(18.0),
          decoration: BoxDecoration(color: Colors.indigo),
          child: Center(
            child: topContentText,
          ),
        ),
      ],
    );

    final content = Container(
      child: Column(
        children: <Widget>[
          Content(title: 'Email', value: user.email),
          Content(title: 'Phone', value: user.phone),
          Content(title: 'Website', value: user.website),
          Content(title: 'Address', value: user.address.street + ', ' + user.address.suite + ', ' + user.address.city + '\n' + user.address.zipcode),
          Content(title: 'Company', value: user.company.name + '\n' + user.company.catchPhrase)
        ],
      )
    );

    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.favorite),
            onPressed: () {},
          ),
          PopupMenuButton(
            icon: Icon(Icons.more_vert),
            itemBuilder: (_) => [
              PopupMenuItem(child: Text('About'))
            ],
          )
        ],
      ),
      body: RefreshIndicator(
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(children: <Widget>[topContent, content])
          ),
          onRefresh: () async => _refreshUser(context)
      ),
    );
  }

}