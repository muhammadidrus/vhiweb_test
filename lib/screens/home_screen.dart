import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vhiweb_test/providers/users.dart';
import 'package:vhiweb_test/widgets/bottom_menu.dart';
import 'package:vhiweb_test/widgets/side_menu.dart';
import 'package:vhiweb_test/widgets/users_list.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Users>(context).fetchAndSetUsers().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _refreshUsers(BuildContext context) async {
    await Provider.of<Users>(context).fetchAndSetUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: Text('User List'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.favorite),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          PopupMenuButton(
            icon: Icon(Icons.more_vert),
            itemBuilder: (_) => [
              PopupMenuItem(child: Text('About'))
            ],
          )
        ],
      ),
      drawer: SideMenu(),
      body: _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : RefreshIndicator(
          child: UserList(),
          onRefresh: () => _refreshUsers(context),
        ),
      bottomNavigationBar: BottomMenu(),
    );
  }
}