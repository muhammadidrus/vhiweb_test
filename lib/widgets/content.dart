import 'package:flutter/material.dart';

class Content extends StatelessWidget {
  final String title;
  final String value;
  const Content({Key key, this.title, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return makeCard(context);
  }

  Card makeCard(BuildContext context) => Card(
    elevation: 0.1,
    child: Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: makeContent(context),
    ),
  );

  ListTile makeContent(BuildContext context) => ListTile(
    title: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title.toUpperCase(),
          style: TextStyle(color: Colors.grey, fontSize: 10.0, fontWeight: FontWeight.bold)
        ),
        Divider(),
        Text(
          value,
          style: TextStyle(color: Colors.grey, fontSize: 14.0),
        ),
      ]
    )
  );

}