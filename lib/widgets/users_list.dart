import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:vhiweb_test/models/user.dart';
import 'package:vhiweb_test/providers/users.dart';
import 'package:vhiweb_test/screens/detail_screen.dart';

class UserList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final usersData = Provider.of<Users>(context);
    final users = usersData.items;

    return ListView.builder(
      itemCount: users.length,
      itemBuilder: (context, index) {
        return makeCard(context, users[index]);
      }
    );
  }

  Card makeCard(BuildContext context, User user) => Card(
    elevation: 0.1,
    child: Container(
      child: makeListTile(context, user),
    ),
  );

  ListTile makeListTile(BuildContext context, User user) => ListTile(
    leading: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(Icons.account_circle, color: Colors.grey),
      ],
    ),
    title: Text(
      user.name,
      style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
    ),

    subtitle: Row(
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Text(user.email, style: TextStyle(color: Colors.grey)),
        )
      ],
    ),
    trailing: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(Icons.favorite, color: Colors.grey),
      ],
    ),
    onTap: () {
      Navigator.of(context).pushNamed(
        DetailScreen.routeName,
        arguments: user.id
      );
    },
  );
}